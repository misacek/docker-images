
include $(shell pwd)/Makefile.variables

MYSELF := TOP/Makefile

.DEFAULT_GOAL := all


help:
	@echo
	@echo Targets: build[-6][-7] tag[-6][-7] push[-6][-7]
	@echo
	@echo Variables:
	@echo 	"DOCKER_HOST (default: $(DOCKER_HOST))"
	@echo 	"IMAGE_VERSION (default: taken from $(IMAGE_VERSION_FILE))"
	@echo 	"IMAGE_VERSION_FILE (default: $(IMAGE_VERSION_FILE))"
	@echo 	"LOCAL_IMAGE_NAME (default: $(LOCAL_IMAGE_NAME)"
	@echo 	"LOGFILE (default: $(LOGFILE))"
	@echo 	"REMOTE_IMAGE_NAME (default: $(REMOTE_IMAGE_NAME))"
	@echo 	"RHEL_MAJOR (default: $(RHEL_MAJOR))"
	@echo 	"RHEL_MINOR (default: 9 for RHEL6, 4 for RHEL7)"
	@echo

all:
	$(MAKE) $(SUBMAKES_PARAMS) build
	$(MAKE) $(SUBMAKES_PARAMS) tag
	$(MAKE) $(SUBMAKES_PARAMS) push
	@echo $(shell date) $(MYSELF): success: $@ target

build-6:
	$(MAKE) build-one RHEL_MAJOR=6
	@echo $(shell date) $(MYSELF): success: $@ target

build-7:
	$(MAKE) build-one RHEL_MAJOR=7
	@echo $(shell date) $(MYSELF): success: $@ target

build-one: check-sudo check-docker
	sudo docker build \
		-t $(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):$(IMAGE_VERSION) \
		-f $(MY_DIR)/Dockerfile-$(RHEL_MAJOR).$(RHEL_MINOR) .
	&>>$(LOGFILE)
	@echo $(shell date) $(MYSELF): success: $@ target for RHEL$(RHEL_MAJOR).$(RHEL_MINOR)

build: build-7 build-6
	@echo $(shell date) $(MYSELF): success: $@ target

tag-6:
	$(MAKE) tag-one RHEL_MAJOR=6
	@echo $(shell date) $(MYSELF): success: $@ target

tag-7:
	$(MAKE) tag-one RHEL_MAJOR=7
	@echo $(shell date) $(MYSELF): success: $@ target

tag-one: check-sudo check-docker
	sudo docker tag \
		$(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):$(IMAGE_VERSION) \
		$(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):latest
	@echo $(shell date) $(MYSELF): success: $@ target for RHEL$(RHEL_MAJOR).$(RHEL_MINOR)

tag: tag-6 tag-7
	@echo $(shell date) $(MYSELF): success: $@ target

push-check: check-sudo check-docker
	# check whether it is possible to download the specified version from
	# docker repository -- it must not suceed because our version should not
	# yet exist
	sudo docker pull $(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):$(IMAGE_VERSION) \
		&>>$(LOGFILE) && /bin/false || /bin/true
	@echo $(shell date) $(MYSELF): success: $@ target

push-6:
	$(MAKE) push-one RHEL_MAJOR=6
	@echo $(shell date) $(MYSELF): success: $@ target

push-7:
	$(MAKE) push-one RHEL_MAJOR=7
	@echo $(shell date) $(MYSELF): success: $@ target

push-one: push-check
	sudo docker push $(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):$(IMAGE_VERSION) \
		&>>$(LOGFILE)
	sudo docker push $(DOCKER_HOST)/$(REMOTE_IMAGE_NAME):latest \
		&>>$(LOGFILE)
	echo $(IMAGE_VERSION) > $(IMAGE_VERSION_FILE)
ifdef HAVE_GIT
	git commit -m 'built new image version: $(IMAGE_VERSION)' $(IMAGE_VERSION_FILE)
endif
	@echo $(shell date) $(MYSELF): success: $@ target for RHEL$(RHEL_MAJOR).$(RHEL_MINOR)

push: push-6 push-7
	@echo $(shell date) $(MYSELF): success: $@ target

